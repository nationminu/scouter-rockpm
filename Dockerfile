FROM redhat-openjdk-18/openjdk18-openshift

MAINTAINER ssong <mwsong@rockplace.co.kr>

USER root
ENV SCOUTER_HOME /home/jboss/scouter/server
ENV SCOUTER_DATA /var/lib/scouter
ENV AGENT_HOST_HOME /home/jboss/scouter/agent.host

ADD  scouter-all-2.7.0.tar.gz /home/jboss/
COPY scouter.openshift.conf ${SCOUTER_HOME}/conf/scouter.conf
COPY scouter.openshift.conf ${AGENT_HOST_HOME}/conf/scouter.conf
COPY rockPM ${SCOUTER_HOME}/extweb/
COPY docker-entrypoint.sh /home/jboss/

RUN chmod 777 ${SCOUTER_HOME} && \
    chmod 777 ${SCOUTER_HOME}/conf && \
    mkdir ${SCOUTER_HOME}/database && \
    chmod 777 ${SCOUTER_HOME}/database && \
    mkdir ${SCOUTER_HOME}/logs && \
    chmod 777 ${SCOUTER_HOME}/logs && \
    chmod 777 ${AGENT_HOST_HOME} && \
    chmod 777 ${AGENT_HOST_HOME}/conf && \
    mkdir ${SCOUTER_DATA} && \
    chown -R 185:root ${SCOUTER_DATA} && \
    chmod 777 ${SCOUTER_DATA} && \
    chown -R 185:root /home/jboss && \
    chmod 777 /home/jboss/docker-entrypoint.sh

EXPOSE 6100/tcp
EXPOSE 6100/udp
EXPOSE 6180/tcp
EXPOSE 6180/udp

USER jboss

CMD ["/home/jboss/docker-entrypoint.sh"]
