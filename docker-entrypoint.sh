#!/usr/bin/env bash

#export AGENT_HOST_HOME=/home/jboss/scouter/agent.host
cd ${AGENT_HOST_HOME};
nohup java  -classpath ./scouter.host.jar scouter.boot.Boot ${SCOUTER_DATA}/host/lib > nohup.out &

#export SCOUTER_HOME=/home/jboss/scouter/server
cd ${SCOUTER_HOME};
java -Xmx512m -classpath ./scouter-server-boot.jar scouter.boot.Boot ./lib
